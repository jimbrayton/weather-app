# WeatherApp application

Ext JS 6.7 for UI: https://docs.sencha.com/extjs/6.7.0/index.html

Weather data from Dark Sky API: https://darksky.net/dev/docs

Weather icons: https://erikflowers.github.io/weather-icons/
