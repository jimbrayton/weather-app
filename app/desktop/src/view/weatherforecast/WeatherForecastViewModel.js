// https://api.darksky.net/forecast/85449c13d7ecb1d822c44730d0f1fb54/44.5439,-73.1479

var apiUrl = 'https://api.darksky.net/forecast'
var apiKey = '85449c13d7ecb1d822c44730d0f1fb54';

var locationLat = 44.5439;
var locationLong = -73.1479;

Ext.define('WeatherApp.view.weatherforecast.WeatherForecastViewModel', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'latitude' },
        { name: 'longitude' },
    ]
});

var currentSubStore = Ext.create('Ext.data.Store', {
    model: 'WeatherApp.view.weatherforecast.WeatherForecastViewModel',
    storeId: 'currentlystore',
    fields: [
        {
            name: 'temperature',
            convert: function (value) {
                return Math.round(value);
            }
        }, {
            name: 'windSpeed',
            convert: function (value) {
                return Math.round(value);
            }
        },{
            name: 'precipProbability',
            convert: function (value) {
                return Math.round(value * 100);
            }
        }
    ],
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'currently'
        }
    }
});
var hourlySubStore = Ext.create('Ext.data.Store', {
    model: 'WeatherApp.view.weatherforecast.WeatherForecastViewModel',
    storeId: 'hourlystore',
    fields: [
        {
            name: 'temperature',
            convert: function (value) {
                return Math.round(value);
            }
        },        {
            name: 'time',
            convert: function (value) {
                var calcHour = new Date(value * 1000);
                return calcHour.toLocaleString('en-US', { hour: 'numeric', hour12: true });
            }
        }
        
    ],
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'hourly.data',
        }
    }
});

var dailySubStore = Ext.create('Ext.data.Store', {
    model: 'WeatherApp.view.weatherforecast.WeatherForecastViewModel',
    storeId: 'dailystore',
    fields: [
        {
            name: 'temperatureHigh',
            convert: function (value) {
                return Math.round(value);
            }
        }, {
            name: 'temperatureLow',
            convert: function (value) {
                return Math.round(value);
            }
        },
        {
            name: 'time',
            convert: function (value) {
                var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                var dayOfTheWeek = (new Date(value * 1000)).getDay();
                return days[dayOfTheWeek];
            }
        }
    ],
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'daily.data',
        }
    }

});

var fullForecastStore = Ext.create('Ext.data.Store', {
    model: 'WeatherApp.view.weatherforecast.WeatherForecastViewModel',
    storeId: 'fullforecaststore',
    autoLoad: true,
    requires: ['Ext.data.proxy.JsonP'],
    proxy: {
        type: 'jsonp',
        url: `${apiUrl}/${apiKey}/${locationLat},${locationLong}`,
        reader: {
            type: 'json',
            rootProperty: ''
        }
    },
    listeners: {
        load: function (store, records, successful) {
            currentSubStore.loadRawData(store.first().data);
            hourlySubStore.loadRawData(store.first().data);
            dailySubStore.loadRawData(store.first().data);
        }
    }
});

// TODO: Is store.first().data the right way to do this?