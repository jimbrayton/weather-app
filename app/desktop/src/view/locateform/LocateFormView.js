Ext.define('WeatherApp.view.locateform.LocateFormView', {
    extend: 'Ext.form.Panel',
    xtype: 'locateformview',
    bodyPadding: 5,
    width: '100%',

    // The form will submit an AJAX request to this URL when submitted
    url: 'http://localhost/save-form.php',

    // Fields will be arranged vertically, stretched to full width
    layout: 'fit',
    // The fields
    defaultType: 'textfield',
    items: [{
        fieldLabel: 'Location Query',
        name: 'locationQuery',
        allowBlank: false
    }],

    // Reset and Submit buttons
    buttons: [{
        text: 'Submit',
        handler: function () {
            console.log(this);
            var form = this.getForm();
            if (form.isValid()) {
                form.submit({
                    success: function (form, action) {
                        Ext.Msg.alert('Success', action.result.msg);
                    },
                    failure: function (form, action) {
                        Ext.Msg.alert('Failed', action.result.msg);
                    }
                });
            }
        }
    }]
});