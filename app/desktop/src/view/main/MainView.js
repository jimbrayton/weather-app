Ext.define('WeatherApp.view.main.MainView', {
  extend: 'Ext.Container',
  xtype: 'mainview',
  requires: [
    'WeatherApp.view.main.MainViewController',
    'WeatherApp.view.main.MainViewModel',
    'WeatherApp.view.weatherforecast.WeatherForecastViewModel'
  ],
  controller: "mainviewcontroller",
  viewModel: {
    type: "mainviewmodel"
  },
  layout: {
    type: 'vbox',
    pack: 'start',
    align: 'stretch',
    width: '300px'
  },
  scrollable: true,
  tabBarPosition: 'bottom',
  items: [{
    xtype: 'locateformview'
  }, {
    title: "Current Conditions",
    xtype: 'currentconditionsview',
    bind: {
      store: '{currentconditions}'
    }
  }, {
    title: "Hourly Forecast",
    xtype: 'hourlyforecastview',
    bind: {
      store: '{hourlyforecast}'
    }
  }, {
    xtype: 'dailyforecastview',
    bind: {
      store: '{dailyforecast}'
    }
  }]
})
