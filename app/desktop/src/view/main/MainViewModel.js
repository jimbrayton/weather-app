Ext.define('WeatherApp.view.main.MainViewModel', {
  extend: 'Ext.app.ViewModel',
  alias: 'viewmodel.mainviewmodel',
  requires: [
    'WeatherApp.view.weatherforecast.WeatherForecastViewModel'
  ],
  stores: {
    fullforecast: Ext.getStore('fullforecaststore'),
    currentconditions: Ext.getStore('currentlystore'),
    hourlyforecast: Ext.getStore('hourlystore'),
    dailyforecast: Ext.getStore('dailystore')
  }
});
