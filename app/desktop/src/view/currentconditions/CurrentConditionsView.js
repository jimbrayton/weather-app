Ext.define('WeatherApp.view.currentconditions.CurrentConditionsView', {
    extend: 'Ext.dataview.DataView',
    xtype: 'currentconditionsview',
    scrollable: true,
    layout: {
        type: 'fit',
    },
    itemTpl: [
        `<div id="currentconditions">
            <div class="currentcity">Colchester, VT</div>
            <div class="currentprimary">
            <i class="wi wi-{icon}"></i>
            <span class="temp">{temperature}&deg;</span> 
            </div>
            <div class="currentsecondary">
            <div class="summary">{summary}</div>
            <i class="wi wi-raindrop"></i>
            <span class="precip">{precipProbability}%</span>&nbsp;&nbsp;
            <i class="wi wi-small-craft-advisory"></i>
            <span class="wind">{windSpeed} mph</span>
            </div>
        </div>`
 
    ]
});