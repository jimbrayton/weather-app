Ext.define('WeatherApp.view.hourlyforecast.HourlyForecastView', {
    extend: 'Ext.dataview.DataView',
    xtype: 'hourlyforecastview',
    
    layout: {
        type: 'hbox',
        pack: 'space-around',

        height: '100%'
    },
    itemTpl: [
        `<div class="singlehour">
        <div>{time}</div>
        <div><i class="wi wi-{icon}"></i></div>
        <div class="hourtemp">{temperature}&deg;</div>
        </div>`
 
    ]
});