Ext.define('WeatherApp.view.dailyforecast.DailyForecastView', {
    extend: 'Ext.grid.Grid',
    xtype: 'dailyforecastview',
    layout: 'vbox',
    height: '100%',
    hideHeaders: true,
    columnLines: false,
    rowLines: false,
    columns: [
        { text: 'Day', dataIndex: 'time', sortable: false, width: '35%' },
        {
            text: 'Icon',
            dataIndex: 'icon',
            sortable: false,
            width: '35%',
            cell: {
                xtype: "gridcell",
                encodeHtml: false,
                align: "center"
            },
            renderer: function (value, record) {
                return `<i class="wi wi-${value}"></i>`;
            }
        },
        {
            text: 'High',
            dataIndex: 'temperatureHigh',
            sortable: false,
            width: '15%',           
            cell: {
                xtype: "gridcell",
                cls: 'temphigh',
                align: "center"
            }
        },
        {
            text: 'Low',
            dataIndex: 'temperatureLow',
            sortable: false,
            width: '15%',           
            cell: {
                xtype: "gridcell",
                cls: 'templow',
                align: "center"
            }
        }
    ]
});

